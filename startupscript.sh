#!/bin/sh

echo "Instalando los requisitos iniciales"
sudo apt-get update
sudo apt-get install \
 apt-transport-https \
 ca-certificates \
 curl \
 gnupg-agent \
 software-properties-common -y
echo "Done"

echo "Instalando la llave"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
echo "Done"

echo "Agregando repositorio de Docker"
sudo add-apt-repository \
 "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
 $(lsb_release -cs) \
 stable"
echo "Done"

echo "Instalando los paquetes"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
echo "Done"

echo "Agregando usuario al grupo acoplable"
groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
echo "Agregado"

echo "version docker"
docker --version

echo "Instalando git"
sudo apt install git
echo "Git instalado"

echo "version git:"
git --version

echo "Instalacion Docker y git EXITOSA"

echo "Reiniciando..."
sudo reboot
